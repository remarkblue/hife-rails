namespace :fix do
  desc "Fixes and media updates"

  task duration_fix: :environment do

    photoSpots = Spot.all
    photoSpots.each do |this_spot|
      this_spot.setDefaultTimeForPhoto
      this_spot.save
    end

  end

  task id2guid: :environment do
    creds = Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'])
    s3 = Aws::S3::Client.new(region: ENV['AWS_REGION'],
                             credentials: creds)
    spots = Spot.all
    spots.each do |spot|
      ext = ''
      case spot.spot_type_id
        when SpotType::PHOTO
          ext = ".jpg"
        when SpotType::AUDIO
          ext = ".wav"
        when SpotType::VIDEO
          ext = ".mp4"
      end


      if ext != ''
        obj = S3_BUCKET.object(spot.id.to_s + ext)
        if obj.exists?
          s3.copy_object(bucket: "hife",
                         copy_source: 'hife/' + spot.id.to_s + ext,
                         key: spot.guid + ext)
          s3.delete_object(bucket: "hife", key: spot.id.to_s + ext)
        end
      end
    end
  end

  task video_transcode: :environment do

    spots = Spot.all
    spots.each do |spot|
      if spot.status_id != Status::COMPLETED &&
          spot.status_id != Status::UNAVAILABLE
        spot.transcode
      end
    end
  end

  task image2hife: :environment do

    spots = Spot.all
    spots.each do |spot|
      if spot.status_id != Status::COMPLETED &&
          spot.status_id != Status::UNAVAILABLE
        spot.imageResize
      end
    end
  end

end
