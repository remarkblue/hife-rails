require 'securerandom'

namespace :seed do
  desc "Put data into the new migrations added"
  task guid: :environment do

    spots = Spot.all
    spots.each do |this_spot|
      if this_spot.guid == nil
        this_spot.guid = SecureRandom.uuid
        this_spot.save
      end
    end

    meetings = Meeting.all
    meetings.each do |this_meeting|
      if this_meeting.guid == nil
        this_meeting.guid = SecureRandom.uuid
        this_meeting.save
      end
    end

  end

end
