awsCreds = Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'])

Aws.config.update({
                      region: ENV['AWS_REGION'],
                      credentials: awsCreds,
                      # I strongly recommend never doing this
                      ssl_verify_peer: false
                  })

S3_BUCKET = Aws::S3::Resource.new.bucket(ENV['S3_BUCKET'])
S3_HIFED_BUCKET = Aws::S3::Resource.new.bucket(ENV['S3_HIFED_BUCKET'])

WEB_MP4_PRESET_ID = '1351620000000-100070'
WEB_FLAC_PRESET_ID = '1351620000001-300110'
WEB_MYFLAC_PRESET_ID = '1465278110979-s1z2kn'

AWS_TRANSCODER = Aws::ElasticTranscoder::Client.new(region: ENV['AWS_REGION'],
                                                    credentials: awsCreds)

S3_SOURCE_URL = 'https://s3.amazonaws.com/hife/'