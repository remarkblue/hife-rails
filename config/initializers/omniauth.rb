OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do

  provider :facebook, ENV['HIFE_FACEBOOK_KEY'], ENV['HIFE_FACEBOOK_SECRET']
           #:scope => 'public_profile,user_events,user_photos,rsvp_event,email,user_birthday'

  provider :twitter, ENV['HIFE_TWITTER_KEY'], ENV['HIFE_TWITTER_SECRET']
  provider :linkedin, ENV['HIFE_LINKEDIN_KEY'], ENV['HIFE_LINKEDIN_SECRET']
  provider :google_oauth2, ENV['HIFE_GOOGLE_KEY'], ENV['HIFE_GOOGLE_SECRET'],
           {
               :name => "google",
               :scope => "email, profile, plus.me, http://gdata.youtube.com",
               :prompt => "select_account",
               :image_aspect_ratio => "square",
               :image_size => 50
           }
  provider :github, ENV['HIFE_GITHUB_KEY'], ENV['HIFE_GITHUB_SECRET']

end
