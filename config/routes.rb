Rails.application.routes.draw do
  get "site/index"

  get "me" => 'member#myProfile'

  get "joinable-meetings" => 'meeting#get_joinable_meetings'
  get "device/:uuid/meetings" => 'meeting#get_device_meetings'

  get "meeting/:guid" => 'meeting#get'
  get "search" => 'search#get'
  get "event/:permalink_id" => 'meeting#permalink'
  put "meeting/:guid" => 'meeting#put'
  match "meeting/:guid" => 'meeting#options', via: :options

  post "spot" => 'spot#post'
  post "join-spot" => 'spot#post'
  post "create-new-meeting-spot" => 'spot#postNewMeeting'

  put "spot" => 'spot#put'

  post "spot-uploaded" => 'spot#uploaded'
  delete "spot/:guid" => 'spot#delete'

  post 'bind-device/:token' => 'member#claimDevice'
  get 'bind-device' => 'member#generateTokenToClaimDevice'

  get "poc/devices" => 'poc#devices'
  get "poc/spots" => 'poc#spots'
  get "poc/meetings" => 'poc#meetings'


  post "sign/in"
  post "sign/up"

  post "sign/facebook"

  get "auth/twitter/callback" => 'sign#twitter'
  get "auth/facebook/callback" => 'sign#facebook'
  get "auth/linkedin/callback" => 'sign#linkedin'
  get "auth/google/callback" => 'sign#google_oauth2'
  get "auth/github/callback" => 'sign#github'

  get "sign/check"

  delete "sign/off"
  match "sign/off" => 'sign#options', via: :options

  root to: "site#index"

  get "company/:action" => "company#:action"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
