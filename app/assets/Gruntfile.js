module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        html2js: {
            main: {
                options: {
                    htmlmin: {
                        collapseWhitespace: true,
                        removeComments: true,
                        removeEmptyAttributes: true,
                        removeRedundantAttributes: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true
                    },
                    rename: function (moduleName) {
                        return moduleName.replace(/^.*\/([^\/]+)\.html$/, function (match, $1) {
                            return "Template." + $1;
                        });
                    },
                    module: 'hife.templates',
                    singleModule: true
                },
                src: ['**/asset/template/*.html'],
                dest: 'javascripts/obj/templates.js'
            }
        },
        watch: {
            html2js: {
                files: [
                    '**/asset/template/*.html'
                ],
                tasks: [
                    'html2js'
                ]
            }
        }

    });

    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-watch');


    // Default task(s).
    grunt.registerTask('default', ['html2js', 'watch']);

};