class MemberController < MarianaController
  layout false

  def generateTokenToClaimDevice
    token = BindToken.new
    token.member = @session_member
    token.save
    render :json => token
  end

  def claimDevice
    if params['token'] && params['device']
      token = BindToken.find_by token: params['token']
      if token && token.status_id == Status::AVAILABLE
        device = Device.find_by uuid: params['device']['uuid']
        if device
          device.member = token.member
          device.save
          token.status_id = Status::VERIFIED
          token.save
          render :json => {
              member: token.member
          }
        end
      end
    end
  end

  def myProfile
    if params['connected_guid'] && params['connected_guid'] != @session_member.guid
      connectedGuid = Member.find_by guid: params['connected_guid']
      @session_member.merge connectedGuid
    end

    render :json => {
        member: @session_member,
        ids: @session_member.ids,
        devices: @session_member.devices
    }
  end

  def options
    render :text => '', :content_type => 'application/json'
  end

end
