class SignController < MarianaController
  layout false

  def twitter
    oauth = request.env['omniauth.auth']
    vendor_found = VendorService.find_by_uid oauth[:uid]
    if vendor_found
      @session_member = vendor_found.member
      common_login_page
    else
      @session_member.name = oauth[:info][:nickname]
      @session_member.email = oauth[:info][:nickname] + " logged.from @twitter.com"
      @session_member.first_name = oauth[:info][:name]
      @session_member.metadata = ""
      @session_member.ranking = 0
      @session_member.status_id = Status::INIT
      @session_member.save

      vendor_found = VendorService.new
      vendor_found.uid = oauth[:uid]
      vendor_found.provider = 'twitter'
      vendor_found.token = oauth[:credentials][:token]
      vendor_found.secret_0 = oauth[:credentials][:secret]
      vendor_found.url = oauth[:info][:urls]['Twitter']
      vendor_found.url_image = oauth[:info][:image]

      #vendor_found.expires = params['auth_response']['expiresIn']

      vendor_found.member = @session_member

      if vendor_found.save
        common_login_page
      end
    end
  end

  def facebook
    oauth = request.env['omniauth.auth']
    vendor_found = VendorService.find_by_uid oauth[:uid]
    if vendor_found
      @session_member = vendor_found.member
      common_login_page
    else
      member_available = Member.find_by_email oauth[:info][:email]
      if member_available
        @session_member = member_available
        unless @session_member.name
          @session_member.name = oauth[:info][:name]
        end
        unless @session_member.email
          @session_member.email = oauth[:info][:email]
        end
        @session_member.status_id = Status::INIT
        @session_member.save
      else
        @session_member.name = oauth[:info][:name]
        @session_member.email = oauth[:info][:email]
        @session_member.metadata = ""
        @session_member.ranking = 0
        @session_member.status_id = Status::INIT
        @session_member.save
      end
      vendor_found = VendorService.new
      vendor_found.uid = oauth[:uid]
      vendor_found.provider = 'facebook'
      vendor_found.token = oauth[:credentials][:token]
      vendor_found.url_image = oauth[:info][:image]
      vendor_found.expires = oauth[:credentials][:expires_at]
      vendor_found.member = @session_member
      if vendor_found.save
        common_login_page
      end
    end
  end

  def linkedin
    oauth = request.env['omniauth.auth']
    vendor_found = VendorService.find_by_uid oauth[:uid]
    if vendor_found
      @session_member = vendor_found.member
      common_login_page
    else
      member_available = Member.find_by_email oauth[:info][:email]
      if member_available
        @session_member = member_available
        unless @session_member.first_name
          @session_member.first_name = oauth[:info][:first_name]
        end
        unless @session_member.last_name
          @session_member.last_name = oauth[:info][:last_name]
        end
        unless @session_member.description
          @session_member.description = oauth[:info][:description]
        end
        unless @session_member.email
          @session_member.email = oauth[:info][:email]
        end
        @session_member.status_id = Status::INIT
        @session_member.save
      else
        @session_member.name = oauth[:info][:name]
        @session_member.first_name = oauth[:info][:first_name]
        @session_member.last_name = oauth[:info][:last_name]
        @session_member.description = oauth[:info][:description]
        @session_member.email = oauth[:info][:email]
        @session_member.metadata = ""
        @session_member.ranking = 0
        @session_member.status_id = Status::INIT
        @session_member.save
      end
      vendor_found = VendorService.new
      vendor_found.uid = oauth[:uid]
      vendor_found.provider = 'linkedin'
      vendor_found.token = oauth[:credentials][:token]
      vendor_found.secret_0 = oauth[:credentials][:secret]
      vendor_found.url_image = oauth[:info][:image]
      vendor_found.member = @session_member
      if vendor_found.save
        common_login_page
      end
    end
  end

  def google_oauth2
    oauth = request.env['omniauth.auth']
    vendor_found = VendorService.find_by_uid oauth[:uid]
    if vendor_found
      @session_member = vendor_found.member
      common_login_page
    else
      member_available = Member.find_by_email oauth[:info][:email]
      if member_available
        @session_member = member_available
        unless @session_member.name
          @session_member.name = oauth[:info][:name]
        end
        unless @session_member.email
          @session_member.email = oauth[:info][:email]
        end
        @session_member.status_id = Status::INIT
        @session_member.save
      else
        @session_member.name = oauth[:info][:name]
        @session_member.email = oauth[:info][:email]
        @session_member.metadata = ""
        @session_member.ranking = 0
        @session_member.status_id = Status::INIT
        @session_member.save
      end
      vendor_found = VendorService.new
      vendor_found.uid = oauth[:uid]
      vendor_found.provider = 'google'
      vendor_found.token = oauth[:credentials][:token]
      vendor_found.url_image = oauth[:info][:image]
      vendor_found.expires = oauth[:credentials][:expires_at]
      vendor_found.member = @session_member
      if vendor_found.save
        common_login_page
      end
    end
  end

  def github
    oauth = request.env['omniauth.auth']
    vendor_found = VendorService.find_by_uid oauth[:uid]
    if vendor_found
      @session_member = vendor_found.member
      common_login_page
    else
      member_available = Member.find_by_email oauth[:info][:email]
      if member_available
        @session_member = member_available
        unless @session_member.name
          @session_member.name = oauth[:info][:name]
        end
        unless @session_member.email
          @session_member.email = oauth[:info][:email]
        end
        @session_member.status_id = Status::INIT
        @session_member.save
      else
        @session_member.name = oauth[:info][:name]
        @session_member.email = oauth[:info][:email]
        @session_member.metadata = ""
        @session_member.ranking = 0
        @session_member.status_id = Status::INIT
        @session_member.save
      end
      vendor_found = VendorService.new
      vendor_found.uid = oauth[:uid]
      vendor_found.provider = 'github'
      vendor_found.token = oauth[:credentials][:token]
      vendor_found.url_image = oauth[:info][:image]
      vendor_found.expires = oauth[:credentials][:expires_at]
      vendor_found.member = @session_member
      if vendor_found.save
        common_login_page
      end

    end
  end

  def oauth_fail
    render :status => :forbidden, :text => (I18n.t 'auth.vendor_oauth_login_failed')
  end

  def up
    error_message = ""
    if @session_member
      @session_member.name = params[:new_member][:name]
      @session_member.email = params[:new_member][:email]
      @session_member.encrypted_password= params[:new_member][:password]
      @session_member.ranking = 0
      @session_member.process_password
      @session_member.status_id = Status::INIT

      if @session_member.save
        session[:member_id] = @session_member.id
        render :json => @session_member.to_json(only: Member::PUBLIC_FIELDS)
        #MemberMailer.welcome(@session_member).deliver
      else
        @session_member.errors.each do |this_error|
          error_message = error_message + Member::HUMANIZED_COLLUMNS[this_error[0]] + " " + this_error[1]
        end
        render :status => :forbidden, :text => (error_message)
      end
    else
      render :status => :forbidden, :text => ('unexpected request from client couldn\'t complete signup')
    end
  end

  def in
    #TODO:Implement remember me
    @session_member = Member.authenticate params[:login]
    common_login
  end

  def off
    session[:member_id] = nil
    @session_member = nil
    render :json => "{ }"
  end

  def options
    render :text => '', :content_type => 'application/json'
  end


  private

  def common_login
    if @session_member
      session[:member_id] = @session_member.id
      render :json => @session_member.to_json(only: Member::PUBLIC_FIELDS, methods: [:thumbnail_url])
    else
      render :status => :forbidden, :text => (I18n.t 'auth.login_failed')
    end
  end

  def common_login_page vendor=nil
    if @session_member
      session[:member_id] = @session_member.id
    end
    redirect_to "http://hife.io/oauth?connected_guid=" + @session_member.guid
  end

end
