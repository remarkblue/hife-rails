#MarianaController
#Born:   x
#Author: Mauricio Inguanzo
#Description:
# Abstract class to enable sharing code between controllers
class MarianaController < ApplicationController
  before_filter :ensure_session_for_everyone, :set_headers

  #run this for every access to make sure we get a strong linkage with every visitor or member
  def ensure_session_for_everyone
    if session[:member_id]
      @session_member = Member.session_member(session[:member_id])
      #It said it was a member but session wasnt found, so log as guest instead
      if @session_member == nil
        @session_member = Member.guest
        session[:member_id] = @session_member.id
      end
    else
      @session_member = Member.guest
      session[:member_id] = @session_member.id
    end

    @pageState = {}

    @metaKeywords = "hife,hife.io,moment,meet,meeting,conference,meetup,stream,conference,event"
    @metaDescription = "Capture Brilliant Minds Key Instants"
    @metaTitle = "hife.io"
  end

  def ensure_no_guest
    if @session_member.status == Status::UNREGISTERED
      render :status => :forbidden, :text => (I18n.t 'auth.permission_denied_for_guest')
    end
  end

  def set_headers
    headers['Access-Control-Allow-Origin'] = 'https://hife.io'
    headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Credentials'] = 'true'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    headers['Access-Control-Max-Age'] = '1728000'


    #response.headers["FACEBOOK_KEY"] = "inguanzotest8373"
    #response.headers["FACEBOOK_SEC"] = "nuevocoloseiso991332398736432"
  end

end
