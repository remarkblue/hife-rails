class CompanyController < MarianaController
  before_filter :ensure_session_for_everyone, :attach_custom_headers
end
