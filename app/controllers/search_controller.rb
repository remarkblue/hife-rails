class SearchController < MarianaController
  layout false

  def get
    entity = params[:entity]
    if entity == nil
      entities = ['Meeting']
    else
      entities = [entity]
    end

    if params[:q] and params[:q].length > 0
      query = params[:q]
      response = Mariana.search entities, query
    else
      response = Mariana.featured entities
    end

    if response
      render :json => {
          object: response
      }
    else
      render :json => {:errors => 'not found'}, :status => 404
    end
    return
  end

end