class SpotController < MarianaController
  layout false

  def uploaded
    if params['uploaded'] and params['uploaded']['key']
      #search for the guid
      guid = params['uploaded']['key'].sub /\..*$/, ''
      spot = Spot.find_by(guid: guid)
      if spot
        spot.adaptContent
        render :json => {success: true}
      else
        render :json => {:errors => 'spot not found'}, :status => 404
      end
    else
      render :json => {:errors => 'bad parameters'}, :status => 400
    end
  end

  def postNewMeeting
    if params['device'] and params['spot']

      Mariana.setCurrentTime params['device']['time'].to_i

      timestamp = Time.at(params['spot']['timestamp'].to_i).to_datetime
      deviceCandidate = Device.find_by_uuid params['device']['uuid']
      unless deviceCandidate
        deviceCandidate = Device.new ({
            uuid: params['device']['uuid'],
            name: params['device']['name'],
            system_name: params['device']['system_name'],
            version: params['device']['version'],
            description: '',
            metadata: '',
            status_id: Status::AVAILABLE
        })
        deviceCandidate.first_activity = timestamp
      end
      deviceCandidate.last_activity = timestamp

      deviceCandidate.save

      meetingCandidate = Meeting.create_mine timestamp, params['spot']['latitude'], params['spot']['longitude'], params['spot']['city']

      if meetingCandidate
        params['spot']['spot_type_id'] = params['spot']['spot_type_id'].to_i

        newSpot = Spot.new({
                               status_id: Status::INIT,
                               last_activity: timestamp,
                               latitude: params['spot']['latitude'],
                               longitude: params['spot']['longitude'],
                               spot_type_id: params['spot']['spot_type_id']})
        newSpot.device = deviceCandidate
        meetingCandidate.addSpot(newSpot)

        render :json => newSpot.details
      else
        render :json => {:errors => 'bad parameters'}, :status => 400
      end
    else
      render :json => {:errors => 'bad parameters'}, :status => 400
    end
  end

  def post
    if params['device'] and params['spot']

      Mariana.setCurrentTime params['device']['time'].to_i

      timestamp = Time.at(params['spot']['timestamp'].to_i).to_datetime
      deviceCandidate = Device.find_by_uuid params['device']['uuid']
      unless deviceCandidate
        deviceCandidate = Device.new ({
            uuid: params['device']['uuid'],
            name: params['device']['name'],
            system_name: params['device']['system_name'],
            version: params['device']['version'],
            description: '',
            metadata: '',
            status_id: Status::AVAILABLE
        })
        deviceCandidate.first_activity = timestamp
      end
      deviceCandidate.last_activity = timestamp

      deviceCandidate.save

      if params['meeting'] and params['meeting']['guid']
        meetingCandidate = Meeting.find_and_update_last_activity timestamp, params['meeting']['guid']
      else
        meetingCandidate = Meeting.find_or_create_mine timestamp, params['spot']['latitude'], params['spot']['longitude'], params['spot']['city']
      end

      if meetingCandidate
        params['spot']['spot_type_id'] = params['spot']['spot_type_id'].to_i

        newSpot = Spot.new({
                               status_id: Status::INIT,
                               last_activity: timestamp,
                               latitude: params['spot']['latitude'],
                               longitude: params['spot']['longitude'],
                               spot_type_id: params['spot']['spot_type_id']})

        newSpot.device = deviceCandidate
        meetingCandidate.addSpot(newSpot)

        render :json => newSpot.details
      else
        render :json => {:errors => 'bad parameters'}, :status => 400
      end
    else
      render :json => {:errors => 'bad parameters'}, :status => 400
    end
  end

  def put
    if params['spot'] and params['spot']['guid']
      Mariana.setCurrentTime params['spot']['timestamp'].to_i
      timestamp = Time.at(Mariana.getCurrentTime).to_datetime
      updatedSpot = Spot.find_by(guid: params['spot']['guid'])
      if updatedSpot
        deviceCandidate = updatedSpot.device
        if deviceCandidate
          deviceCandidate.last_activity = timestamp
          deviceCandidate.save
          updatedSpot.last_activity = timestamp
          updatedSpot.save
        end
        meeting = updatedSpot.meeting
        if meeting
          meeting.last_activity = timestamp
          meeting.save
        end
        render :json => updatedSpot.details
      else
        render :json => {:errors => 'spot not found'}, :status => 404
      end
    else
      render :json => {:errors => 'bad parameters'}, :status => 400
    end
  end

  def delete
    spot = Spot.find_by(guid: params['guid'])
    if spot
      spot.destroy
      render :json => spot.details
    else
      render :json => {:errors => 'spot not found'}, :status => 404
    end
  end

end
