class PocController < ApplicationController
  layout false

  def devices
    found_devices = Device.all
    render :json => found_devices
  end

  def meetings
    found_meetings = Meeting.all
    render :json => found_meetings
  end

  def spots
    found_spots = Spot.all
    render :json => found_spots
  end


end
