class MeetingController < MarianaController
  layout false

  def permalink
    meeting = Meeting.find_by(permalink_id: params['permalink_id'])
    if meeting
      Mariana.setCurrentTime params['time'].to_i

      render :json => meeting.details
    else
      render :json => {:errors => 'meeting not found'}, :status => 404
    end
  end

  def get
    meeting = Meeting.find_by(guid: params['guid'])
    if meeting
      Mariana.setCurrentTime params['time'].to_i

      render :json => meeting.details
    else
      render :json => {:errors => 'meeting not found'}, :status => 404
    end
  end

  def get_device_meetings
    found = []
    device = Device.find_by(uuid: params['uuid'])
    if device
      meetings = device.previous_meetings
      meetings.each do |meeting|
        found.push meeting.details
      end
      render :json => {
          object: found
      }
    else
      render :json => {:errors => 'meeting not found'}, :status => 404
    end
  end

  def get_joinable_meetings
    found = []
    timestamp = Time.at(params['spot']['timestamp'].to_i).to_datetime
    meetings = Meeting.find_joinable timestamp,
                                     params['spot']['latitude'],
                                     params['spot']['longitude']
    meetings.each do |meeting|
      found.push meeting.details
    end
    render :json => {
        object: found
    }
  end

  def put
    changed = false
    meeting = Meeting.find_by(guid: params['guid'])
    if meeting
      prev_name = meeting.name
      if params['name']
        changed = true
        meeting.name = params['name']
        if prev_name != meeting.name and meeting.permalink_id == nil
          meeting.setPermalink
        end
      end
      if params['description']
        changed = true
        meeting.description = params['description']
      end
      if params['image']
        changed = true
        meeting.image = params['image']
      end
      if params['banner']
        changed = true
        meeting.banner = params['banner']
      end

      if (params['status_id']) #TODO: need to check if membership and ownership is in place to close meeting
        changed = true
        meeting.status_id = params['status_id'].to_i
      end

      if changed
        if meeting.save
          render :json => meeting.details
        else
          render :json => {:errors => 'meeting failed to save'}, :status => 500
        end
      else
        render :json => {:errors => 'missing parameters'}, :status => 400
      end
    else
      render :json => {:errors => 'meeting not found'}, :status => 404
    end
  end

  def options
    render :text => '', :content_type => 'application/json'
  end

end
