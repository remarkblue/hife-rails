require 'securerandom'

class Spot < Mariana

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'Spot'
  TABLE_NAME = 'spots'
  ENTITY_NAME = 'spot'
  READABLE_NAME = 'Spot'
  DB_DESCRIPTION = 'Spot'

  REMOVE_DEFAUL_TIMESTAMP = true

  ##################################################
  #Model fields
  FIELDS = {
      :id => [Mariana::SQL_INTEGER],
      :meeting_id => [Mariana::SQL_INTEGER],
      :device_id => [Mariana::SQL_INTEGER],
      :spot_type_id => [Mariana::SQL_INTEGER],

      :status_id => [Mariana::SQL_INTEGER],

      :first_activity => [Mariana::SQL_DATE_TIME],
      :last_activity => [Mariana::SQL_DATE_TIME],

      :latitude => [Mariana::SQL_DECIMAL, {:precision => 10, :scale => 6}],
      :longitude => [Mariana::SQL_DECIMAL, {:precision => 10, :scale => 6}],

      :name => [Mariana::SQL_STRING],
      :url => [Mariana::SQL_STRING],
      :ranking => [Mariana::SQL_INTEGER],
      :metadata => [Mariana::SQL_TEXT]
  }

  #PUBLIC_FIELDS = [:name, :description, :ranking, :powers, :status_id]

  ##################################################
  #Model associations

  belongs_to :meeting
  belongs_to :device

  def initialize(params)
    super
    self.guid = SecureRandom.uuid
    if self.spot_type_id == SpotType::PHOTO
      self.setDefaultTimeForPhoto
    else
      if self.instant
        self.first_activity = self.last_activity
      else
        self.first_activity = self.last_activity - 15.seconds
      end
    end
  end

  def transcode
    transcoding = ''
    ext = ''
    case self.spot_type_id
      when SpotType::AUDIO
        ext = ".wav"
        transcoding = WEB_MP4_PRESET_ID
      when SpotType::VIDEO
        ext = ".mp4"
        transcoding = WEB_MP4_PRESET_ID
    end

    if ext != ''
      obj = S3_BUCKET.object(self.guid.to_s + ext)
      if obj.exists?
        AWS_TRANSCODER.create_job(
            pipeline_id: '1460426395989-n52zmq',
            input: {
                key: self.guid + ext,
                frame_rate: 'auto',
                resolution: 'auto',
                aspect_ratio: 'auto',
                interlaced: 'auto',
                container: 'auto'
            },
            output: {
                key: self.guid,
                preset_id: transcoding,
                thumbnail_pattern: "",
                rotate: '0'
            }
        )
        self.status_id = Status::COMPLETED
        print 'DONE:' + self.guid + ext
      else
        self.status_id = Status::UNAVAILABLE
        print 'UNAVAILABLE:' + self.guid + ext
      end
      self.save
    end
  end

  def imageResize
    ext = ''
    if self.spot_type_id == SpotType::PHOTO
      ext = ".jpg"
    end

    if ext != ''
      obj = S3_BUCKET.object(self.guid.to_s + ext)
      if obj.exists?

        image = MiniMagick::Image.open(S3_SOURCE_URL + self.guid.to_s + ext)

        image.auto_orient
        image.resize "800x800"
        obj = S3_HIFED_BUCKET.object self.guid + '_800'
        obj.put body: image.to_blob

        image.resize "300x300"
        obj = S3_HIFED_BUCKET.object self.guid + '_400'
        obj.put body: image.to_blob

        image.resize "100x100"
        obj = S3_HIFED_BUCKET.object self.guid + '_200'
        obj.put body: image.to_blob
        self.status_id = Status::COMPLETED
        print 'DONE:' + self.guid + ext
      else
        self.status_id = Status::UNAVAILABLE
        print 'UNAVAILABLE:' + self.guid + ext
      end
      self.save
    end
  end

  def adaptContent
    case self.spot_type_id
      when SpotType::PHOTO
        self.imageResize
      when SpotType::VIDEO
        self.transcode
      when SpotType::AUDIO
        self.transcode
    end
  end

  def setDefaultTimeForPhoto
    if self.spot_type_id == SpotType::PHOTO
      self.first_activity = self.last_activity - 5.seconds
      self.last_activity = self.last_activity + 5.seconds
    end
  end

  def instant
    return (spot_type_id == SpotType::IN or
        self.spot_type_id == SpotType::OUT or
        self.spot_type_id == SpotType::AUDIO or
        self.spot_type_id == SpotType::VIDEO)
  end

  def details
    myMeeting = meeting
    myDevice = device
    (myNeighbors, myMomentMetric) = myMeeting.neighbors_and_moments myDevice

    return {
        :spot_type => SpotType::AT[self.spot_type_id],
        :spot => self,
        :moment_metric => myMomentMetric,
        :device => myDevice,
        :neighbors => myNeighbors,
        :meeting => myMeeting
    }
  end

  def subDetails
    myDevice = device

    return {
        :duration => Mariana.duration(self.last_activity, self.first_activity),
        :spot_type => SpotType::AT[self.spot_type_id],
        :spot => self,
        :device => myDevice
    }
  end

end
