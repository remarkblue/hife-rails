class Device < Mariana

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'Device'
  TABLE_NAME = 'devices'
  ENTITY_NAME = 'device'
  READABLE_NAME = 'Device'
  DB_DESCRIPTION = 'Device'

  REMOVE_DEFAUL_TIMESTAMP = true

  ##################################################
  #Model fields
  FIELDS = {
      :id => [Mariana::SQL_INTEGER],
      :uuid => [Mariana::SQL_STRING],
      :name => [Mariana::SQL_STRING],
      :system_name => [Mariana::SQL_STRING],
      :version => [Mariana::SQL_STRING],
      :description => [Mariana::SQL_TEXT],
      :status_id => [Mariana::SQL_INTEGER],
      :metadata => [Mariana::SQL_TEXT],

      :member_id => [Mariana::SQL_INTEGER],

      :first_activity => [Mariana::SQL_DATE_TIME],
      :last_activity => [Mariana::SQL_DATE_TIME]
  }

  has_many :spots
  belongs_to :member

  attr_accessor :meetingSpotsCount

  def previous_meetings
    meetings_found = []
    meeting_ids = []

    sortedSpots = spots.sort { |x, y| x.first_activity <=> y.first_activity }

    sortedSpots.each do |this_spot|
      meeting = this_spot.meeting
      if meeting.status_id == Status::COMPLETED
        index = meeting_ids.index meeting.id
        if index == nil
          meetings_found.push meeting
          meeting_ids.push meeting.id
        end
      end
    end

    return meetings_found
  end

end
