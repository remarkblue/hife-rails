require 'securerandom'

class Meeting < Mariana
  searchkick word_start: [:name, :description, :metadata]

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'Meeting'
  TABLE_NAME = 'meetings'
  ENTITY_NAME = 'meeting'
  READABLE_NAME = 'meeting'
  DB_DESCRIPTION = 'meeting'

  MOMENT_HOLDER_TIME = 7
  ##################################################
  #Model fields

  REMOVE_DEFAUL_TIMESTAMP = true

  FIELDS = {
      :id => [Mariana::SQL_INTEGER],
      :name => [Mariana::SQL_STRING],
      :description => [Mariana::SQL_TEXT],

      :meeting_type_id => [Mariana::SQL_INTEGER],
      :spot_id => [Mariana::SQL_INTEGER],

      :first_activity => [Mariana::SQL_DATE_TIME],
      :last_activity => [Mariana::SQL_DATE_TIME],

      :latitude => [Mariana::SQL_DECIMAL, {:precision => 10, :scale => 6}],
      :longitude => [Mariana::SQL_DECIMAL, {:precision => 10, :scale => 6}],

      :size => [Mariana::SQL_INTEGER],
      :ranking => [Mariana::SQL_INTEGER],
      :status_id => [Mariana::SQL_INTEGER],
      :metadata => [Mariana::SQL_TEXT]
  }

  #natural spots
  has_many :spots

  #extracted from spot
  belongs_to :spot


  def self.find_joinable (timestamp, latitude, longitude)
    joinable = []
    activeMeetings = Meeting.where(status_id: Status::AVAILABLE)
    if activeMeetings.kind_of?(Meeting)
      activeMeetings = [activeMeetings]
    end
    #check by proximity
    if activeMeetings
      activeMeetings.each do |meetingInstance|
        if meetingInstance.is_hosting timestamp, latitude, longitude
          joinable.push meetingInstance
        end
      end
    end

    #TODO: check by invitation

    return joinable
  end

  def self.create_mine (timestamp, latitude, longitude, placeholderPrefixName="")
    linkedMeeting = Meeting.new ({
        guid: SecureRandom.uuid,

        name: placeholderPrefixName + ' ' + timestamp.strftime("%b %d, %Y"),

        first_activity: timestamp,
        last_activity: timestamp,

        size: 0,
        status_id: Status::AVAILABLE,

        latitude: latitude,
        longitude: longitude
    })
    linkedMeeting.save

    return linkedMeeting
  end

  def self.find_and_update_last_activity timestamp, guid
    meeting = self.find_by guid: guid
    if meeting
      if meeting.last_activity.to_i < timestamp.to_i
        meeting.last_activity = timestamp
        meeting.save
      end
      return meeting
    else
      return nil
    end
  end

  def self.find_or_create_mine (timestamp, latitude, longitude, placeholderPrefixName="")
    linkedMeeting = nil
    activeMeetings = Meeting.where(status_id: Status::AVAILABLE)
    if activeMeetings.kind_of?(Meeting)
      activeMeetings = [activeMeetings]
    end

    if activeMeetings
      activeMeetings.each do |meetingInstance|
        if meetingInstance.is_hosting timestamp, latitude, longitude
          linkedMeeting = meetingInstance
          if linkedMeeting.last_activity.to_i < timestamp.to_i
            linkedMeeting.last_activity = timestamp
            linkedMeeting.save
          end
          break
        end
      end
    end

    unless linkedMeeting
      linkedMeeting = Meeting.new ({
          guid: SecureRandom.uuid,

          name: placeholderPrefixName + ' ' + timestamp.strftime("%b %d, %Y"),

          first_activity: timestamp,
          last_activity: timestamp,

          size: 0,
          status_id: Status::AVAILABLE,

          latitude: latitude,
          longitude: longitude
      })
      linkedMeeting.save
    end
    return linkedMeeting
  end

  def setPermalink
    self.permalink_id = self.name.gsub /[^a-z^A-Z^0-9^_^-]/, '-'
    self.ranking = self.ranking + 2
    save
  end

  def details
    devices_found = []
    devices_ids = []
    duration = Mariana.duration last_activity, first_activity
    richSpots = []

    sortedSpots = spots.sort { |x, y| x.first_activity <=> y.first_activity }

    if self.ranking == nil
      self.ranking = 0
    end
    self.ranking = self.ranking + (0.1 * spots.length)


    #TODO: Code clean up needed here
    sortedSpots.each do |this_spot|
      device = this_spot.device
      richSpots.push(this_spot.subDetails)
      index = devices_ids.index device.id
      if index == nil
        device.meetingSpotsCount = 1
        devices_found.push device
        devices_ids.push device.id
        self.ranking = self.ranking + 1
      else
        devices_found[index].meetingSpotsCount = devices_found[index].meetingSpotsCount + 1
      end
    end
    save

    (myNeighbors, myMomentMetric) = self.neighbors_and_moments


    return {
        :meeting => self,
        :duration => duration,
        :spots => richSpots,
        :devices => devices_found,
        :moment_metric => myMomentMetric
    }
  end

  def neighbors_and_moments myDevice = nil
    devicesFound = []
    currentMomentMetrics = {
        :cool => 0,
        :what => 0,
        :agree => 0,
        :disagree => 0,
        :photo => 0,
        :video => 0,
        :audio => 0
    }

    currentTime = Mariana.getCurrentTime
    devicesIdFound = []
    meetingSpots = self.spots
    meetingSpots.each do |spot|
      # print("---------")
      # print(Time.now.to_s + " = " + currentTime.to_s)
      # print("   ><   "  + spot.last_activity.to_s + " = " + spot.last_activity.to_i.to_s  + "---------\n")
      if (spot.last_activity.to_i - currentTime).abs < MOMENT_HOLDER_TIME
        #print("----ACTIVE SPOT-----")
        case spot.spot_type_id
          when SpotType::WHAT
            currentMomentMetrics[:what] = currentMomentMetrics[:what] + 1
          when SpotType::COOL
            currentMomentMetrics[:cool] = currentMomentMetrics[:cool] + 1
          when SpotType::AGREE
            currentMomentMetrics[:agree] = currentMomentMetrics[:agree] + 1
          when SpotType::NOAGREE
            currentMomentMetrics[:disagree] = currentMomentMetrics[:disagree] + 1
          when SpotType::PHOTO
            currentMomentMetrics[:photo] = currentMomentMetrics[:photo] + 1
          when SpotType::AUDIO
            currentMomentMetrics[:audio] = currentMomentMetrics[:audio] + 1
          when SpotType::VIDEO
            currentMomentMetrics[:video] = currentMomentMetrics[:video] + 1
        end
      end
      if myDevice != nil and (not devicesIdFound.include? spot.device_id) and spot.device_id != myDevice.id
        devicesFound.push(spot.device)
        devicesIdFound.push(spot.device_id)
      end
    end
    return [devicesFound, currentMomentMetrics]
  end

  def is_hosting timestamp, latitude, longitude
    if status_id != Status::COMPLETED
      if (timestamp.to_i - self.last_activity.to_i).abs < 3600 #Is it within 1hr?
        #Is it within 100mts?
        deltamts = distance(latitude.to_f,
                            longitude.to_f,
                            self.latitude.to_f,
                            self.longitude.to_f)
        if deltamts < 100
          return true
        end
      end
    end
    return false
  end


  def addSpot newSpot
    newSpot.meeting = self
    if newSpot.first_activity < self.first_activity
      newSpot.first_activity = self.first_activity
    end

    newSpot.save
    self.size = self.size + 1

    if newSpot.spot_type_id == SpotType::PHOTO
      if self.image
        if !self.banner
          self.banner = newSpot.name
        end
      else
        self.image = newSpot.name
      end
    end
    save
  end

  def distance lat0, long0, lat1, long1
    loc1 = [lat0, long0]
    loc2 = [lat1, long1]
    rad_per_deg = Math::PI/180 # PI / 180
    rkm = 6371 # Earth radius in kilometers
    rm = rkm * 1000 # Radius in meters

    dlat_rad = (lat0-lat1) * rad_per_deg # Delta, converted to rad
    dlon_rad = (long0-long1) * rad_per_deg

    lat1_rad, lon1_rad = loc1.map { |i| i * rad_per_deg }
    lat2_rad, lon2_rad = loc2.map { |i| i * rad_per_deg }

    a = Math.sin(dlat_rad/2)**2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin(dlon_rad/2)**2
    c = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1-a))

    return (rm * c).abs
  end

end
