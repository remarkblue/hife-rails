class MeetingType

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'MeetingType'
  TABLE_NAME = 'meeting_types'
  ENTITY_NAME = 'meeting_type'
  READABLE_NAME = 'Meeting Type'
  DB_DESCRIPTION = 'Meeting Type'

  AT = {
      "present" => {
          :id => 0
      }
  }

end
