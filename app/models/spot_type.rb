class SpotType

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'SpotType'
  TABLE_NAME = 'spot_types'
  ENTITY_NAME = 'spot_type'
  READABLE_NAME = 'Spot Type'
  DB_DESCRIPTION = 'Spot Type'

  IN = 0
  OUT = 1
  WHAT = 2
  NOAGREE = 3
  AGREE = 4
  COOL = 5
  START = 7
  FINISH = 8
  AUDIO = 20
  VIDEO = 21
  PHOTO = 22


  AT = {
      IN => "in",
      OUT => "out",
      WHAT => "what",
      NOAGREE => "noagree",
      AGREE => "agree",
      COOL => "cool",
      AUDIO => "audio",
      VIDEO => "video",
      PHOTO => "photo",
      START => "begin",
      FINISH => "end"
  }


end
