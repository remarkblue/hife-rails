require 'bcrypt'
require 'securerandom'

class Member < Mariana

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'Member'
  TABLE_NAME = 'members'
  ENTITY_NAME = 'member'
  READABLE_NAME = 'Members'
  DB_DESCRIPTION = 'community member'

  ##################################################
  #Model fields
  FIELDS = {
      :id => [Mariana::SQL_INTEGER],
      :name => [Mariana::SQL_STRING],
      :gender => [Mariana::SQL_BOOLEAN],
      :email => [Mariana::SQL_STRING],
      :first_name => [Mariana::SQL_STRING],
      :middle_name => [Mariana::SQL_STRING],
      :last_name => [Mariana::SQL_STRING],
      :encrypted_password => [Mariana::SQL_STRING],
      :description => [Mariana::SQL_TEXT],
      :balance_id => [Mariana::SQL_INTEGER],
      :ranking => [Mariana::SQL_INTEGER],
      :status_id => [Mariana::SQL_INTEGER],
      :memberships_member_id => [Mariana::SQL_INTEGER],

      :metadata => [Mariana::SQL_TEXT]

  }

  PUBLIC_FIELDS = [:name, :description, :ranking, :powers, :status_id]


  ##################################################
  #Model associations

  HUMANIZED_COLLUMNS = {
      :name => "User name",
      :email => "e-mail",
      :first_name => "First name",
      :last_name => "Last name"
  }

  has_many :vendor_services
  has_many :bind_tokens
  has_many :devices

  ##################################################
  #Model validations
  NAME_MIN_LENGTH = 2
  NAME_MAX_LENGTH = 120
  PASSWORD_MIN_LENGTH = 4
  PASSWORD_MAX_LENGTH = 40
  EMAIL_MAX_LENGTH = 200
  NAME_RANGE = NAME_MIN_LENGTH..NAME_MAX_LENGTH
  PASSWORD_RANGE = PASSWORD_MIN_LENGTH..PASSWORD_MAX_LENGTH
  # Text box sizes for display in the views
  NAME_SIZE = 20
  PASSWORD_SIZE = 10
  PASSWORD_FIELD_SIZE = 12

  ##################################################
  #Model default entries


  ##################################################
  #Model methods

  def self.schema_based_on_model migration_obj, explicit_id=false
    super
    migration_obj.add_index TABLE_NAME, :name, unique: true
    migration_obj.add_index TABLE_NAME, :email, unique: true
  end


  #Used to support sign in through the same read call on the web service
  def self.is_sign_granted? item
    if item.class.to_s == 'Member' && item.id > 0
      return true
    else
      return false
    end
  end

  #check sign in for pair +name+ and +entry_password+
  def self.authenticate(params)
    if this_subscriber = self.find_by_email(params[:email])
      current=BCrypt::Password.new(this_subscriber.encrypted_password)
      if current == params[:password]
        return this_subscriber
      else
        return nil
      end
    end
    return nil
  end

  #---------------------------------------------------------------------------------------------
  # Constructor and Initialization functionality

  def initialization hashin=nil
    super hashin
    @is_guest = false
  end

  def merge connectedGuid
    if connectedGuid
      vendorService = connectedGuid.vendor_services
      if vendorService
        vendorService.each do |vs|
          vs.member = self
          vs.save
        end
      end

      if connectedGuid.first_name
        self.first_name = connectedGuid.first_name
      end
      if connectedGuid.last_name
        self.last_name = connectedGuid.last_name
      end
      if connectedGuid.email
        self.email = connectedGuid.email
      end
      if connectedGuid.description
        self.description = connectedGuid.description
      end
      connectedGuid.destroy

      if self.status_id == Status::UNREGISTERED
        self.status_id = Status::INIT
      end
      self.save
    end
  end

  #create a new member
  def self.guest
    this_guest = Member.new
    this_guest.name = Mariana.generate_name
    this_guest.status_id = Status::UNREGISTERED
    this_guest.guid = SecureRandom.uuid

    if this_guest.save
      return this_guest
    else
      return nil
    end
  end

  def self.signed?(this_member)
    if this_member
      if DeploySettings.forced_member?
        if !this_member.is_guest?
          return true
        end
      else
        return true
      end
    end
    return false
  end

  #Generates a complete member name
  def full_name
    return "#{first_name}" + ((middle_name and middle_name!="") ? " #{middle_name}" : "") +
        ((last_name and last_name!="") ? " #{last_name}" : "")
  end

  #Generates a complete member name and override the readable name common menuCook method
  def readable_name
    self.full_name
  end

  #provide a default administrator member use for generic updates where no member can be identified properly
  def self.admin
    return self.get(self::DEFAULT_MEMBER)
  end

  #provide a default administrator member use for generic updates where no member can be identified properly
  def self.tester
    return self.get(self::TESTER_MEMBER)
  end

  def thumbnail_url
    vs = self.vendor_services
    if vs and vs.length > 0
      vs.each do |vendor|
        if vendor.provider == 'facebook' and vendor.uid
          return "https://graph.facebook.com/" + vendor.uid + "/picture?type=square"
        end
        if vendor.provider == 'twitter'
          return vendor.url_image
        end
      end
    else
      return nil
    end
    return nil
  end

  def ids
    response = []
    services = self.vendor_services
    services.each do |thisService|
      response.push({
                        provider: thisService.provider,
                        url_image: thisService.url_image
                    })
    end
    return response
  end

  #---------------------------------------------------------------------------------------------

  #---------------------------------------------------------------------------------------------
  # Load functionality

  #pre load default entries based on DEFAULT_VALUES Hash constant for fixtures used during testing
  def self.load_fixtures
    fixtures_string = ""
    self::DEFAULT_VALUES.each do |system_name, entries|
      fixtures_string=fixtures_string + "#{system_name}:\n"
      fixtures_string=fixtures_string + "  system_name: #{system_name}\n"
      fixtures_string=fixtures_string + "  encrypted_password: #{BCrypt::Password.create(entries[0], :cost => 10)}\n"
      fixtures_string=fixtures_string + "  first_name: #{entries[1]}\n"
      fixtures_string=fixtures_string + "  middle_name: #{entries[2]}\n"
      fixtures_string=fixtures_string + "  last_name: #{entries[3]}\n"
      fixtures_string=fixtures_string + "  emails: #{entries[4]}\n"
      fixtures_string=fixtures_string + "  balance: #{system_name}\n\n"
    end

    fixtures_string=fixtures_string + "crap_1:\n"
    fixtures_string=fixtures_string + "  system_name: crap_1\n"
    fixtures_string=fixtures_string + "  encrypted_password: #{BCrypt::Password.create("crap_1", :cost => 10)}\n"
    fixtures_string=fixtures_string + "  first_name: Crap\n"
    fixtures_string=fixtures_string + "  last_name: One\n"
    fixtures_string=fixtures_string + "  emails: crap_1\n"
    fixtures_string=fixtures_string + "  balance: crap_1\n\n"

    fixtures_string=fixtures_string + "crap_2:\n"
    fixtures_string=fixtures_string + "  system_name: crap_2\n"
    fixtures_string=fixtures_string + "  encrypted_password: #{BCrypt::Password.create("crap_2", :cost => 10)}\n"
    fixtures_string=fixtures_string + "  first_name: Crap\n"
    fixtures_string=fixtures_string + "  last_name: Two\n"
    fixtures_string=fixtures_string + "  emails: crap_2\n"
    fixtures_string=fixtures_string + "  balance: crap_2\n\n"
    return fixtures_string
  end

  #---------------------------------------------------------------------------------------------
  # Password functionality

  #encrypt password to be stored for the member
  def process_password
    password = BCrypt::Password.create(self.encrypted_password, :cost => 10)
    self.encrypted_password = password
  end

  #request this member to start process to reset its password
  def reset_password
    if self.status_id != Status::PENDING
      self.collaboration_case = false
      self.is_guest = true
      self.unencrypted_password=Mariana.generate_password
      this_encryption=BCrypt::Password.create(self.unencrypted_password, :cost => 10)
      self.encrypted_password=this_encryption
      self.status_id = Status::RESET
      if self.save
        begin
          MemberMailer.deliver_forgot(self)
        rescue StandardError => bang
          Mariana.debug("Critical error trying to send e-mail to #{self.system_name}:" + bang)
          #TODO:4: How do we mark an action item to resend password?
        end
        return true
      end
      Mariana.debug("Unexpected error at saving time:#{self.errors.full_messages}")
    else
      Mariana.debug("member[#{self.id}]:#{self.system_name} accound on pending state is not allowed to reset password")
    end
    return false
  end

  #re stablish password and recover member status
  def recover_password
    self.collaboration_case = false
    self.status_id = Status::AVAILABLE
    if self.save
      return true
    end
    Mariana.debug("Unexpected error at saving time:#{self.errors.full_messages}")
    return false
  end


  #static:session_member:
  def Member.session_member(member_id)
    member_found = nil
    if member_id and member_id > 0
      begin
        member_found=Member.find(member_id)
      rescue
        Mariana.debug("[RESCUE]Member not found: id=#{member_id}")
      end
    end
    return member_found
  end

  #Verifies if the +action_object+ is allowed by +self+ member on +entity_class+
  # +action_object+:: Action instance requested to be verified for permission on self member
  # +entity_class+:: menuCook Class to be evaluated on this verification
  # +returns+:: Boolean: <b>true</b> if permission is granted, <b>false</b> otherwise
  def allow_action_on_entity(action_object, entity_class)
    #TODO:4: implement security
    return true
  end

  #Verifies if the +action_object+ is allowed by +self+ member on +entry+ object
  # +action_object+:: Action instance requested to be verified for permission on self member
  # +entry+:: menuCook object to be evaluated for permission
  # +returns+:: Boolean: <b>true</b> if permission is grated, <b>false</b> otherwise
  def allow_action_on_entry(action_obj, entry)
    #TODO:4: implement security
    return true
  end

  #validate if member is allowed to edit this_object
  # +this_object+:: Instance evaluated for permissions to edit
  def is_authorized_to_edit(this_object)
    if self.is_guest?
      return false
    else
      return true
    end
  end

  # start a member object in the database to request access permission
  # +member_name+:: string to state the system_name
  # +return+:: new member instance requested
  def self.create_from_sign_up_request(member_name)
    new_member_request = self.new
    new_member_request.system_name = member_name
    new_member_request.is_guest = true
    new_member_request.collaboration_case = false
    new_member_request.unencrypted_password=Mariana.generate_password
    this_encryption=BCrypt::Password.create(new_member_request.unencrypted_password, :cost => 10)
    new_member_request.encrypted_password=this_encryption
    new_member_request.status_id = Status::PENDING
    if new_member_request.save
      return new_member_request
    else
      Mariana.debug "Error trying to create a member from a request access event"
      message = ""
      new_member_request.errors.each do |this_error|
        message = message + this_error[1]
      end
      return ((message == "") ? nil : message)
    end
  end

  #---------------------------------------------------------------------------------------------

end
