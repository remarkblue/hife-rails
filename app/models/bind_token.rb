require 'securerandom'

class BindToken < Mariana

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'BindToken'
  TABLE_NAME = 'bind_tokens'
  ENTITY_NAME = 'bind_token'
  READABLE_NAME = 'Bind Token'
  DB_DESCRIPTION = 'Bind Token'

  ##################################################
  #Model fields
  FIELDS = {
      :id => [Mariana::SQL_INTEGER],
      :token => [Mariana::SQL_STRING],
      :member_id => [Mariana::SQL_INTEGER],
      :status_id => [Mariana::SQL_INTEGER]
  }

  belongs_to :member


  def initialize
    super
    self.token = (SecureRandom.hex 2).to_s.upcase
    self.status_id = Status::AVAILABLE
  end

end
