class Mariana < ActiveRecord::Base
  self.abstract_class = true

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'Mariana'

  REMOVE_DEFAUL_TIMESTAMP = false

  PASSWORD_LENGTH = 8
  NAME_LENGTH = 20

  MIN_NAME = 4
  MIN_DESCRIPTION = 15

  MAXIMUM_SPONSORS_BY_SECTION = 8
  MAXIMUM_DISPLAY_BY_SECTION = 30
  MAXIMUM_DISPLAY = 60

  SEARCH_LEVEL = "2"

  SQL_INTEGER = 'integer'
  SQL_REF = 'ref'
  SQL_STRING = 'string'
  SQL_TEXT = 'text'
  SQL_DATE = 'date'
  SQL_BOOLEAN = 'boolean'
  SQL_FLOAT = 'float'
  SQL_DOUBLE = 'double'
  SQL_REAL = 'real'
  SQL_DECIMAL = 'decimal'
  SQL_DATE_TIME = 'datetime'

  @@current_time = (Time.now).to_i


  def self.setCurrentTime value = (Time.now).to_i
    @@current_time = value
  end

  def self.getCurrentTime
    return @@current_time
  end

  ##################################################
  # Web Services functionality

  # web service receiver for reading CRUD request
  def self.web_read parameters, default_member=nil
    results = Array.new
    if parameters
      if parameters['pattern'] and parameters['pattern'].length > 0
        results = self.find_with_ferret(parameters['pattern'])
      else
        if parameters['ids']
          if parameters['ids'].class.to_s == 'Array'
            parameters['ids'].each do |this_id|
              item = self.find(this_id)
              results.push(item) if item
            end
          else
            item = self.find(parameters['ids'])
            results.push(item) if item
          end
        else
          results = self.all
        end
      end
    end
    return results
  end

  # web service receiver for reading CRUD request
  def self.web_delete parameters, default_member=nil
    results = self.web_read parameters, default_member
    #TODO: verify permissions!
    if results
      results.each do |this_item|
        self.delete(this_item)
      end
    end
    return results
  end

  # web service receiver for reading CRUD request
  def self.web_update parameters, default_member=nil
    results = Array.new
    if parameters and parameters['id'] and default_member
      item = self.find(parameters['id'])
      if item
        parameters.each do |this_param, this_value|
          item.send(this_param + "=", this_value) if (this_param != 'id')
        end
        if item.save
          results.push(item)
        end
      end
    else
      results = self.web_create parameters, default_member
    end
    return results
  end

  # web service receiver for create CRUD request
  def self.web_create parameters, default_member=nil
  end

  def self.search entities, query
    response = []
    for entityItem in entities
      class_obj = Object.const_get(entityItem)
      matches = class_obj.search query, fields: [{name: :word_start,
                                                  description: :word_start}]
      for item in matches
        response.push item.details
      end

    end

    return response
    #concat, set type and sort by intention
  end

  def self.featured entities
    response = []

    for entityItem in entities
      class_obj = Object.const_get(entityItem)
      
      matches = class_obj.order('ranking').last(10).reverse
      for item in matches
        response.push item.details
      end

    end

    return response
  end

  # Load functionality
  #pre load default entries based on DEFAULT_VALUES Hash constant
  def self.load_fixtures
    fixtures_string = ""
    if self.const_defined?('DEFAULT_VALUES')
      self::DEFAULT_VALUES.each do |entry|
        fixtures_string=fixtures_string + "#{entry[:system_name]}:\n"
        fixtures_string=fixtures_string + "  system_name: #{entry[:system_name]}\n"
        fixtures_string=fixtures_string + "  readable_name: #{entry[:readable_name]}\n"
        fixtures_string=fixtures_string + "  description: #{entry[:description]}\n\n"
      end
    end
    return fixtures_string
  end

  #cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
  #   Mayor.create(:name => 'Daley', :city => cities.first)
  #pre load default entries based on DEFAULT_VALUES Hash constant
  def self.prepopulate
    if self.const_defined?('DEFAULT_VALUES')
      self.create(self::DEFAULT_VALUES)
    end
  end

  #static method used to define models fields in the model app and get the configuration on the DB migration automatically
  def self.schema_based_on_model(migration_obj, explicit_id=false)
    if explicit_id
      # schema implementation with explicit ID declaration
      migration_obj.create_table self::TABLE_NAME, :id => false do |t|
        self::FIELDS.each do |this_field, these_attributes|
          if these_attributes.length == 2
            t.column this_field, these_attributes[0], these_attributes[1]
          else
            t.column this_field, these_attributes[0]
          end

        end
        unless self::REMOVE_DEFAUL_TIMESTAMP
          t.timestamps
        end
      end
      # end of schema implementation
    else
      # schema implementation with implicit ID declaration
      migration_obj.create_table self::TABLE_NAME do |t|
        self::FIELDS.each do |this_field, these_attributes|
          if this_field.to_s != 'id'
            t.column this_field, these_attributes[0]
          end
        end
        unless self::REMOVE_DEFAUL_TIMESTAMP
          t.timestamps
        end
      end
      # end of schema implementation
    end

  end

  #---------------------------------------------------------------------------------------------

  #---------------------------------------------------------------------------------------------
  # Framework helpers

  def self.duration last_activity, first_activity
    sec = last_activity - first_activity
    duration = " "
    if sec < 60
      duration = "~ 1min"
    else
      hrs =(sec/3600).round
      min = (sec/60).round - (hrs * 60)
      if hrs > 1
        duration = hrs.to_s + " hrs "
      end
      if hrs == 1
        duration = "1 hr "
      end
      if min > 0
        duration = duration + min.to_s + " min "
      end
    end
    return duration
  end

  def self.build_path path_to_evaluate
    unless File.directory?(path_to_evaluate)
      array_of_dirs = path_to_evaluate.split "/"
      subdir = ""
      array_of_dirs.each do |this_dir|
        subdir = subdir + "/" + this_dir
        Dir.mkdir(subdir) unless File.directory?(subdir)
      end
    end
  end

  #generate password
  def self.generate_password
    chars = ('a' .. 'z').to_a + ('A' .. 'Z').to_a + ('1' .. '9').to_a + '%_$?@!'.split(//)
    Array.new(PASSWORD_LENGTH, '').collect { chars[rand(chars.size)] }.join
  end

  #generate name
  def self.generate_name
    chars = ('a' .. 'z').to_a + ('A' .. 'Z').to_a + ('1' .. '9').to_a
    Array.new(NAME_LENGTH, '').collect { chars[rand(chars.size)] }.join
  end

  #generate name
  def self.generate_safe_name
    chars = ('a' .. 'z').to_a
    Array.new(5, '').collect { chars[rand(chars.size)] }.join
  end

  #method to emulate readable_name entity per entity
  def self.readable_name(association)
    if (association.class.to_s == Supplier::SYSTEM_NAME)
      return self::READABLE_NAME_SUPPLIER
    else
      return self::READABLE_NAME
    end
  end

  def self.save_upload(entityClass, upload, file_name=nil)

    unless file_name
      file_name = upload.original_filename
    end
    file_name = entityClass::ENTITY_NAME + '_' + file_name

    # Make an object in your bucket for your upload
    obj = S3_BUCKET.object file_name
    #obj = s3.bucket('bucket-name').object('key')
    # string data
    obj.put body: upload.read


    foodImage = entityClass.new ({
        name: file_name,
        source: obj.public_url})
    foodImage.member = @session_member
    foodImage.save


    return {
        id: foodImage.id,
        entitiy_name: entityClass::ENTITY_NAME,
        path: obj.public_url,
        file_name: file_name
    }
  end

  #---------------------------------------------------------------------------------------------
  # Logging methods

  #Write out into the log as a debug comment
  def self.debug comment=""
    logger.info "<MARIANA>#{comment}</MARIANA>"
  end

  #---------------------------------------------------------------------------------------------
  # Test helpers

  #new an object with valid values for the model
  def self.create_a_valid_test_instance
    return self.new(
        :system_name => "mauricioInguanzo",
        :readable_name => "Mauricio inguanzo",
        :description => "This is the programmer and creator of Mariana!")
  end

  #take an object from the class and make crap out of it
  def self.get_a_valid_test_instance
    return self.get(get_a_valid_test_instance_system_name)
  end

  #take another object from the class and make crap out of it
  def self.get_second_valid_test_instance
    return self.get(get_second_valid_test_instance_system_name)
  end

  #provide the system name requested by this class
  def self.get_a_valid_test_instance_system_name
    return "crap_1"
  end

  #provide the second system name requested by this class
  def self.get_second_valid_test_instance_system_name
    return "crap_2"
  end

  #---------------------------------------------------------------------------------------------


  ##################################################
  #Model default entries  
  # SYSTEM_NAME  => CLASS
  DEFAULT_VALUES = {

  }

  # NAME  => CLASS
  DEFAULT_ENTITIES = {

  }

  #TABLE_NAME => CLASS
  DEFAULT_TABLES = {

  }

  #DB_MIGRATION => CLASS
  DEFAULT_MIGRATION_IDS = {

  }

  ##################################################
  #Model methods

  # static method to generate the class based on +this_target+
  # +this_target+:: variable to look up menuCook clas that could be the class migration, class name, class table or class entity
  # +returns+:: Array: of actions allowed by +user_obj+ or +nil+ if there is any conflict on parameters provided
  def self.get_class_obj(this_target)
    if this_target != nil and this_target != ""
      target = this_target.to_s
      return DEFAULT_MIGRATION_IDS[target] if DEFAULT_MIGRATION_IDS[target]
      return DEFAULT_VALUES[target] if DEFAULT_VALUES[target]
      return DEFAULT_TABLES[target] if DEFAULT_TABLES[target]
      return DEFAULT_ENTITIES[target] if DEFAULT_ENTITIES[target]
      possible_integer = target.to_i
      if possible_integer and possible_integer > 0
        target = sprintf("%03d", target.to_i)
        return DEFAULT_MIGRATION_IDS[target] if DEFAULT_MIGRATION_IDS[target]
      end
    end
    return nil
  end

  #Static method to generate the object based on the DBID and the obj id
  def self.get_obj(this_dbid, this_id=nil)
    this_object = nil
    if this_dbid and this_dbid.to_i > 0
      this_class=MenucookEntity.get_class_obj(this_dbid)
      if this_class
        if this_id
          this_object = this_class.find(this_id.to_i)
        else
          this_object = this_class.new
        end
      end
    end
    return this_object
  end

  def self.get name
    return self.find_by_name name
  end


end
