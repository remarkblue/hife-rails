class Entity < Mariana
  self.abstract_class = true

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'Entity'

  def as_json(options={})
    super.as_json(options).merge({searchEntityType: self.class::SYSTEM_NAME })
  end

end
