class VendorService < Mariana

  ##################################################
  #Model Constants
  SYSTEM_NAME = 'VendorService'
  TABLE_NAME = 'vendor_services'
  ENTITY_NAME = 'vendor_service'
  READABLE_NAME = 'Vendor Service'
  DB_DESCRIPTION = 'Vendor Service'
  DB_MIGRATION = ''
  DB_ID = 0

  ##################################################
  #Model fields
  FIELDS = {
      :id => [Mariana::SQL_INTEGER],
      :member_id => [Mariana::SQL_INTEGER],
      :provider => [Mariana::SQL_STRING],
      :token => [Mariana::SQL_STRING],
      :secret_0 => [Mariana::SQL_STRING],
      :secret_1 => [Mariana::SQL_STRING],

      :expires => [Mariana::SQL_DATE_TIME],

      :uid => [Mariana::SQL_STRING],
      :url => [Mariana::SQL_STRING],
      :url_image => [Mariana::SQL_STRING]
  }

  belongs_to :member

end
