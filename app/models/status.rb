class Status

  INIT=1
  BOOKED=2
  NOTIFIED=3
  PROGRESS=4
  COMPLETED=5
  VERIFIED=6
  REVIEWED=7
  CANCELED=8
  CHANGE=9
  FORCED=10
  SKIP=11
  PENDING=12
  UPDATED=13
  AVAILABLE=14
  UNAVAILABLE=15
  UNREGISTERED=16
  BYOWNER=17

  row = {
      init: {
          id: INIT,
          name: 'init',
          description: ''
      },
      booked: {
          id: BOOKED,
          name: 'booked',
          description: ''
      },
      notified: {
          id: NOTIFIED,
          name: 'notified',
          description: ''
      },
      progress: {
          id: PROGRESS,
          name: 'progress',
          description: ''
      },
      completed: {
          id: COMPLETED,
          name: 'completed',
          description: ''
      },
      verified: {
          id: VERIFIED,
          name: 'verified',
          description: ''
      },
      reviewed: {
          id: REVIEWED,
          name: 'reviewed',
          description: ''
      },
      canceled: {
          id: CANCELED,
          name: 'canceled',
          description: ''
      },
      change: {
          id: CHANGE,
          name: 'change',
          description: ''
      },
      forced: {
          id: FORCED,
          name: 'forced',
          description: ''
      },
      skip: {
          id: SKIP,
          name: 'skip',
          description: ''
      },
      pending: {
          id: PENDING,
          name: 'pending',
          description: ''
      },
      updated: {
          id: UPDATED,
          name: 'updated',
          description: ''
      },
      available: {
          id: AVAILABLE,
          name: 'available',
          description: ''
      },
      unavailable: {
          id: UNAVAILABLE,
          name: 'unavailable',
          description: ''
      },
      unregistered: {
          id: UNREGISTERED,
          name: 'unregistered',
          description: ''
      },
      posted_by_owner: {
          id: BYOWNER,
          name: 'Posted by owner',
          description: 'This item was submitted by owner.'
      }
  }


end
