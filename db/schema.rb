# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160524071433) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bind_tokens", force: :cascade do |t|
    t.string   "token"
    t.integer  "member_id"
    t.integer  "status_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "devices", force: :cascade do |t|
    t.string   "uuid"
    t.string   "name"
    t.string   "system_name"
    t.string   "version"
    t.text     "description"
    t.integer  "status_id"
    t.text     "metadata"
    t.integer  "member_id"
    t.datetime "first_activity"
    t.datetime "last_activity"
  end

  create_table "meetings", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "meeting_type_id"
    t.integer  "spot_id"
    t.datetime "first_activity"
    t.datetime "last_activity"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.integer  "size"
    t.integer  "ranking"
    t.integer  "status_id"
    t.text     "metadata"
    t.string   "image"
    t.string   "banner"
    t.string   "guid"
    t.string   "permalink_id"
  end

  create_table "members", force: :cascade do |t|
    t.string   "name"
    t.boolean  "gender"
    t.string   "email"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "encrypted_password"
    t.text     "description"
    t.integer  "balance_id"
    t.integer  "ranking"
    t.integer  "status_id"
    t.integer  "memberships_member_id"
    t.text     "metadata"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "guid",                  null: false
    t.string   "zip"
    t.string   "state"
    t.string   "city"
    t.string   "country"
    t.boolean  "on_web"
    t.integer  "on_meeting_id"
    t.string   "cell"
  end

  add_index "members", ["email"], name: "index_members_on_email", unique: true, using: :btree
  add_index "members", ["guid"], name: "index_members_on_guid", unique: true, using: :btree
  add_index "members", ["name"], name: "index_members_on_name", unique: true, using: :btree

  create_table "spots", force: :cascade do |t|
    t.integer  "meeting_id"
    t.integer  "device_id"
    t.integer  "spot_type_id"
    t.integer  "status_id"
    t.datetime "first_activity"
    t.datetime "last_activity"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.string   "name"
    t.string   "url"
    t.integer  "ranking"
    t.text     "metadata"
    t.string   "guid"
    t.text     "description"
    t.string   "permalink_id"
  end

  create_table "vendor_services", force: :cascade do |t|
    t.integer  "member_id"
    t.string   "provider"
    t.string   "token"
    t.string   "secret_0"
    t.string   "secret_1"
    t.datetime "expires"
    t.string   "uid"
    t.string   "url"
    t.string   "url_image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
