class MeetingImagesFields < ActiveRecord::Migration
  def change
    add_column :meetings, :image, :string
    add_column :meetings, :banner, :string
  end
end
