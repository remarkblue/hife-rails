class MemberAdjustments2 < ActiveRecord::Migration
  def change
    add_column :members, :zip, :string
    add_column :members, :state, :string
    add_column :members, :city, :string
    add_column :members, :country, :string
    add_column :members, :on_web, :boolean
    add_column :members, :on_meeting_id, :integer
    add_column :members, :cell, :string
  end
end
