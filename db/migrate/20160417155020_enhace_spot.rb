class EnhaceSpot < ActiveRecord::Migration
  def change
    add_column :spots, :description, :text
    add_column :spots, :permalink_id, :string, :unique => true
  end
end
