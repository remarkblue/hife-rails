class Permalink < ActiveRecord::Migration

  def change
    add_column :meetings, :permalink_id, :string, :unique => true
  end

end
