class MemberAdjustments < ActiveRecord::Migration
  def change
    add_column :members, :guid, :string,
               { :unique => true, :null => false }
    add_index :members, :guid, unique: true
  end
end
