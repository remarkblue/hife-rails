class AddingGuidFields < ActiveRecord::Migration

    def change
      add_column :meetings, :guid, :string, :unique => true
      add_column :spots, :guid, :string, :unique => true
    end

end
